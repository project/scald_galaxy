; This is a standard make file for packaging the distribution along with any
; contributed modules/themes or external libraries. Some examples are below.
; See http://drupal.org/node/159730 for more details.

api = 2
core = 7.x
defaults[projects][subdir] = ''

; Contributed modules; standard.
projects[responsive_bartik][type] = theme
projects[responsive_bartik][version] = 1.0

; Contributed modules: admin.
projects[admin_menu][version] = 3.0-rc5
projects[devel][version] = 1.5
projects[module_filter][version] = 1.8

; Contributed modules: 3rd add-ons
projects[breakpoints][version] = 1.3
projects[direct_upload][version] = 1.x-dev
projects[picture][version] = 2.9
projects[plupload][version] = 1.7
projects[imagefield_crop][version] = 1.1

projects[ctools][version] = 1.7
projects[entity][version] = 1.6
projects[views][version] = 3.11
projects[quickedit][version] = 1.1
projects[libraries][version] = 2.2
projects[ckeditor][version] = 1.16
projects[scald][version] = 1.x-dev
projects[scald_dailymotion][version] = 1.4
projects[scald_flickr][version] = 1.2
projects[scald_gallery][version] = 2.4
projects[scald_soundcloud][version] = 1.1
projects[scald_twitter][version] = 1.0-rc6
projects[scald_vimeo][version] = 1.4
projects[scald_youtube][version] = 1.4
projects[statuses][version] = 1.0-beta2
projects[statuses_scald][version] = 1.x-dev

; Libraries
libraries[plupload][download][type] = get
libraries[plupload][download][url] = https://github.com/moxiecode/plupload/archive/v1.5.8.zip
libraries[plupload][directory_name] = plupload
libraries[plupload][destination] = "libraries"
libraries[plupload][patch][] = http://drupal.org/files/issues/plupload-1_5_8-rm_examples-1903850-16.patch

libraries[galleria][download][type] = get
libraries[galleria][download][url] = http://galleria.io/static/galleria-1.4.2.zip
libraries[galleria][directory_name] = galleria
libraries[galleria][destination] = libraries

libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.0%20Beta/ckeditor_4.5.0_beta_standard_all.zip

libraries[underscore][download][type] = get
libraries[underscore][download][url] = https://github.com/jashkenas/underscore/archive/1.5.2.zip

libraries[backbone][download][type] = get
libraries[backbone][download][url] = https://github.com/jashkenas/backbone/archive/1.1.0.zip
